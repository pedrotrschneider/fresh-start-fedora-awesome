[2021/10/21 11:46:03] ===============================================
[2021/10/21 11:46:03] ===== Ventoy2Disk 1.0.61 127.0.0.1:24680 =====
[2021/10/21 11:46:03] ===============================================
[2021/10/21 11:46:03] ventoy_disk_enumerate_all
[2021/10/21 11:46:03] get disk info sdb
[2021/10/21 11:46:03] get disk size from sysfs for sdb
[2021/10/21 11:46:03] ventoy_get_vtoy_data /dev/sdb
[2021/10/21 11:46:03] Invalid efi part2 name <>
[2021/10/21 11:46:03] disk:</dev/sdb 8:16> model:<ATA KINGSTON SA400S3 (scsi)> size:240057409536 (240 GB)
[2021/10/21 11:46:03] /dev/sdb NO Ventoy detected
[2021/10/21 11:46:03] get disk info sdc
[2021/10/21 11:46:03] get disk size from sysfs for sdc
[2021/10/21 11:46:03] ventoy_get_vtoy_data /dev/sdc
[2021/10/21 11:46:03] Not valid ventoy partition layout [64 6117888] [6117952 8192]
[2021/10/21 11:46:03] disk:</dev/sdc 8:32> model:<SanDisk Cruzer Blade (USB)> size:15382609920 (16 GB)
[2021/10/21 11:46:03] /dev/sdc NO Ventoy detected
[2021/10/21 11:46:03] get disk info sda
[2021/10/21 11:46:03] get disk size from sysfs for sda
[2021/10/21 11:46:03] ventoy_get_vtoy_data /dev/sda
[2021/10/21 11:46:03] Invalid efi part2 name <>
[2021/10/21 11:46:03] disk:</dev/sda 8:0> model:<ATA WDC WD10JPVX-22J (scsi)> size:1000204886016 (1024 GB)
[2021/10/21 11:46:03] /dev/sda NO Ventoy detected
[2021/10/21 11:46:03] ============= DISK DUMP ============
[2021/10/21 11:46:03] /dev/sdc [16 GB] SanDisk Cruzer Blade (USB)	Ventoy: NA
[2021/10/21 11:46:03] /dev/sda [1024 GB] ATA WDC WD10JPVX-22J (scsi)	Ventoy: NA
[2021/10/21 11:46:03] /dev/sdb [240 GB] ATA KINGSTON SA400S3 (scsi)	Ventoy: NA
[2021/10/21 11:46:07] ventoy_disk_enumerate_all
[2021/10/21 11:46:07] get disk info sdb
[2021/10/21 11:46:07] get disk size from sysfs for sdb
[2021/10/21 11:46:07] ventoy_get_vtoy_data /dev/sdb
[2021/10/21 11:46:07] Invalid efi part2 name <>
[2021/10/21 11:46:07] disk:</dev/sdb 8:16> model:<ATA KINGSTON SA400S3 (scsi)> size:240057409536 (240 GB)
[2021/10/21 11:46:07] /dev/sdb NO Ventoy detected
[2021/10/21 11:46:07] get disk info sdc
[2021/10/21 11:46:07] get disk size from sysfs for sdc
[2021/10/21 11:46:07] ventoy_get_vtoy_data /dev/sdc
[2021/10/21 11:46:07] Not valid ventoy partition layout [64 6117888] [6117952 8192]
[2021/10/21 11:46:07] disk:</dev/sdc 8:32> model:<SanDisk Cruzer Blade (USB)> size:15382609920 (16 GB)
[2021/10/21 11:46:07] /dev/sdc NO Ventoy detected
[2021/10/21 11:46:07] get disk info sda
[2021/10/21 11:46:07] get disk size from sysfs for sda
[2021/10/21 11:46:07] ventoy_get_vtoy_data /dev/sda
[2021/10/21 11:46:07] Invalid efi part2 name <>
[2021/10/21 11:46:07] disk:</dev/sda 8:0> model:<ATA WDC WD10JPVX-22J (scsi)> size:1000204886016 (1024 GB)
[2021/10/21 11:46:07] /dev/sda NO Ventoy detected
[2021/10/21 11:46:29] ==================================================================================
[2021/10/21 11:46:29] ===== ventoy install /dev/sdc style:MBR secureboot:0 align4K:1 reserve:0 =========
[2021/10/21 11:46:29] ==================================================================================
[2021/10/21 11:46:29] /dev/sdc mounted </dev/sdc1 /media/ptrschneider/MANJARO_GNOME_2116 iso9660 ro,nosuid,nodev,relatime,nojoliet,check=s,map=n,blocksize=2048,uid=1000,gid=1000,dmode=500,fmode=400,iocharset=utf8 0 0
>
[2021/10/21 11:46:29] disk is mounted, now try to unmount it ...
[2021/10/21 11:46:29] umount /dev/sdc /media/ptrschneider/MANJARO_GNOME_2116 [ success ]
[2021/10/21 11:46:29] disk is not mounted now, we can do the install ...
[2021/10/21 11:46:29] start install thread SanDisk Cruzer Blade (USB) ...
[2021/10/21 11:46:29] ventoy_install_thread run ...
[2021/10/21 11:46:29] check disk sdc
[2021/10/21 11:46:29] disk is not mounted now, we can do continue ...
[2021/10/21 11:46:29] ventoy_clean_disk fd:5 size:15382609920
[2021/10/21 11:46:29] write disk at off:0 writelen:65536 datalen:65536
[2021/10/21 11:46:29] write disk at off:15382544384 writelen:65536 datalen:65536
[2021/10/21 11:46:29] read core.img.xz rc:0 len:455168
[2021/10/21 11:46:29] ventoy_unxz_stg1_img len:455168 rc:0
[2021/10/21 11:46:29] read disk.img.xz rc:0 len:12573172
[2021/10/21 11:46:30] ventoy_unxz_efipart_img len:12573172 rc:0 unxzlen:33554432
[2021/10/21 11:46:30] Fill MBR part table
[2021/10/21 11:46:30] Disk signature: 0x110646d3
[2021/10/21 11:46:30] no need to align with 4KB
[2021/10/21 11:46:30] ReservedSector: 0
[2021/10/21 11:46:30] Part1StartSector:2048 Part1SectorCount:29976576 Part2StartSector:29978624
[2021/10/21 11:46:30] Formatting part1 exFAT /dev/sdc ...
[0121/10/21 11:46:30] Creating... 
[0121/10/21 11:46:30] Flushing... 
[2021/10/21 11:46:30] Formatting part2 EFI ...
[2021/10/21 11:46:30] Formatting part2 EFI offset:15349055488 ...
[2021/10/21 11:46:30] VentoyProcSecureBoot 0 ...
[2021/10/21 11:46:30] Open ventoy efi file 0x635ae0 
[2021/10/21 11:46:30] ventoy efi file size 1753088 ...
[2021/10/21 11:46:30] Now delete all efi files ...
[2021/10/21 11:46:30] Open bootx64 efi file 0x635ae0 
[2021/10/21 11:46:31] Open ventoy efi file 0x635f30
[2021/10/21 11:46:31] ventoy efi file size 1179648 ...
[2021/10/21 11:46:31] Now delete all efi files ...
[2021/10/21 11:46:31] Open bootia32 efi file 0x635ae0
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] write disk writelen:1048576 datalen:1048576 [ success ]
[2021/10/21 11:46:31] Writing legacy grub ...
[2021/10/21 11:46:31] Write MBR stage1 ...
[2021/10/21 11:46:31] lseek offset:512(512) writelen:1048064(1048064)
[2021/10/21 11:46:31] fsync data1...
[2021/10/21 11:46:33] Checking part2 efi data /dev/sdc ...
[2021/10/21 11:46:34] efi part data check success
[2021/10/21 11:46:34] Writting Partition Table style:0...
[2021/10/21 11:46:34] Writting MBR Partition Table 0 512
[2021/10/21 11:46:34] fsync data2...
[2021/10/21 11:46:34] ====================================
[2021/10/21 11:46:34] ====== ventoy install success ======
[2021/10/21 11:46:34] ====================================
[2021/10/21 11:46:34] ventoy_disk_enumerate_all
[2021/10/21 11:46:34] get disk info sdb
[2021/10/21 11:46:34] get disk size from sysfs for sdb
[2021/10/21 11:46:34] ventoy_get_vtoy_data /dev/sdb
[2021/10/21 11:46:34] Invalid efi part2 name <>
[2021/10/21 11:46:34] disk:</dev/sdb 8:16> model:<ATA KINGSTON SA400S3 (scsi)> size:240057409536 (240 GB)
[2021/10/21 11:46:34] /dev/sdb NO Ventoy detected
[2021/10/21 11:46:34] get disk info sdc
[2021/10/21 11:46:34] get disk size from sysfs for sdc
[2021/10/21 11:46:34] ventoy_get_vtoy_data /dev/sdc
[2021/10/21 11:46:34] ventoy partition layout check OK: [2048 29976576] [29978624 65536]
[2021/10/21 11:46:34] now check secure boot for /dev/sdc ...
[2021/10/21 11:46:34] /EFI/BOOT/grubx64_real.efi not exist
[2021/10/21 11:46:34] disk:</dev/sdc 8:32> model:<SanDisk Cruzer Blade (USB)> size:15382609920 (16 GB)
[2021/10/21 11:46:34] /dev/sdc Ventoy:<1.0.61> MBR secureboot:0 preserve:0
[2021/10/21 11:46:34] get disk info sda
[2021/10/21 11:46:34] get disk size from sysfs for sda
[2021/10/21 11:46:34] ventoy_get_vtoy_data /dev/sda
[2021/10/21 11:46:34] Invalid efi part2 name <>
[2021/10/21 11:46:34] disk:</dev/sda 8:0> model:<ATA WDC WD10JPVX-22J (scsi)> size:1000204886016 (1024 GB)
[2021/10/21 11:46:34] /dev/sda NO Ventoy detected
[2021/10/21 11:47:11] ventoy server exit due to signal ...

# Fresh start Fedora 36

Repository containing a collection of scripts for a fresh install on Fedora 36 Workstation with GNOME and Awesome Window Manager.

## Sumary
- [Post installation](#post-installation)
- [Fish setup](#change-default-shell-to-fish)
- [Config files setup](#setup-config-files)
- [Software setup](#setup-all-needed-software)
    - [Build Jonaburg's Picom](#build-and-install-jonaburgs-picom)
    - [Build Btop](#build-and-install-btop)
    - [Build Digimend Kernel Drivers](#build-and-install-digimend-kernel-drivers)
    - [Build Betterlockscreen](#build-and-install-better-lockscreen)
    - [Build Libinput Gestures](#build-and-install-libinput-gestures)
    - [Build EWW](#build-and-install-elkowars-wacky-widgets)
    - [Install WezTerm](#install-wezterm)
    - [Insall aditional software](#install-additional-software)
- [Visual Studio Codium Setup](#setup-visual-studio-codium)
- [Build Wifi Receiver Driver](#install-wifi-receiver-driver)
- [System configuration](#system-configuration)
    - [Xfce session and startup](#xfce-session-and-startup)
    - [GIT setup](#git-setup)
- [Squashing some bugs](#squashing-some-bugs)
    - [SELinux warnings](#selinux-warnings)
    - [Backlight control problems](#backlight-control-not-working)
    - [Ranger icons not working](#ranger-icons-not-working)
- [Resources](#resources)

## Post installation

Start by editing ```/etc/dnf/dnf.conf``` by adding these lines at the end:

```
fastestmirror=True
max_parallel_downloads=10
defaultyes=True
keepcache=True
```

Then, update your system.
```bash
sudo dnf update
```

Enable free and non-free repositories on rpm fusion.
```bash
sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf groupupdate core
```

Add flathub source to flatpaks.
```bash
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

Change hostname.
```bash
sudo hostnamectl set-hostname "paul"
```

Install media codecs.
```bash
sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
sudo dnf groupupdate sound-and-video
```

## Install fish
```bash
sudo dnf install fish util-linux-user
```

Now, add the ```fish``` command to your ~/.bashrc file. You can also change the default shell of the system with ```chsh -s `which fish````, but I don't recommend that since fish isn't POSIX complient.

## Setup config files

Dot files.
```bash
cp -r .config/* ~/.config
```

Icons, themes and fonts.
```bash
cp -r .icons ~/
cp -r .themes ~/
cp -r .fonts ~/
sudo cp -r .themes/Nordic-Darker /usr/share/themes/
```

Wallpapers.
```bash
cp -r wallpapers ~/Pictures/
sudo mkdir -p /usr/share/wallpapers
sudo cp ~/Pictures/wallpapers/* /usr/share/wallpapers/
sudo cp .config/neofetch/mountain.png /usr/share/wallpapers/
```

Some program binaries.
```bash
cp -r programs ~/Documents/
```

Some utility programs
```bash
cp -r utils/* ~/.local/bin/
```

## Setup all needed software

### Build and install [Jonaburg's picom](https://github.com/jonaburg/picom)

Install the dependencies
```bash
sudo dnf groupinstall "Development Tools" "Development Libraries"
sudo dnf install @development-tools
sudo dnf install dbus-devel gcc g++ git libconfig-devel libdrm-devel libev-devel libX11-devel libX11-xcb libXext-devel libxcb-devel mesa-libGL-devel meson pcre-devel pixman-devel uthash-devel xcb-util-image-devel xcb-util-renderutil-devel xorg-x11-proto-devel cmake ninja-build
```

Clone and build the package
```bash
cd
mkdir .src
cd .src
git clone https://github.com/jonaburg/picom
cd picom
meson --buildtype=release . build
ninja -C build
sudo ninja -C build install
```

### Build and install [Btop](https://github.com/aristocratos/btop)

Clone and build the package
```bash
cd
cd .src
git clone https://github.com/aristocratos/btop.git btop
cd btop
make
sudo make install
```

### Build and install [Digimend Kernel Drivers](https://github.com/DIGImend/digimend-kernel-drivers)

Install dependencies
```bash
sudo dnf install -y "kernel-devel-uname-r == $(uname -r)"
sudo dnf install -y dkms
```

Clone and build the package
```bash
cd ~/.src
git clone https://github.com/DIGImend/digimend-kernel-drivers.git digimend-kernel-drivers
cd digimend-kernel-drivers
sudo make dkms_install
```

### Build and install [Better Lockscreen](https://github.com/betterlockscreen/betterlockscreen#installation-script)

Install dependencies
```bash
sudo dnf install -y autoconf automake cairo-devel fontconfig gcc libev-devel libjpeg-turbo-devel libXinerama libxkbcommon-devel libxkbcommon-x11-devel libXrandr pam-devel pkgconf xcb-util-image-devel xcb-util-xrm-devel xset
```

Clone and build i3lock-color
```bash
cd ~/.src
git clone https://github.com/Raymo111/i3lock-color.git
cd i3lock-color
./install-i3lock-color.sh
```

Clone and build better lockscreen
```bash
cd ~/.src
wget https://github.com/pavanjadhaw/betterlockscreen/archive/refs/heads/main.zip
unzip main.zip
cd betterlockscreen-main/

chmod u+x betterlockscreen
sudo cp betterlockscreen /usr/local/bin/
sudo cp system/betterlockscreen@.service /usr/lib/systemd/system/
systemctl enable betterlockscreen@$USER

betterlockscreen -u ~/Pictures/wallpapers/wallpaper-nordic.jpg
```

### Build and install [Libinput Gestures](https://github.com/bulletmark/libinput-gestures)

Create and add yourself to the input user group.
```bash
sudo gpasswd -a $USER input
newgrp input
```

Install dependencies.
```bash
sudo dnf install wmctrl xdotool
```

Install Libinput Gestures.
```bash
cd ~/.src
git clone https://github.com/bulletmark/libinput-gestures.git
cd libinput-gestures
sudo make install
```

### Build and install [Elkowars Wacky Widgets](https://github.com/elkowar/eww)

Install dependencies.
```bash
sudo dnf install cairo-gobject-devel pango-devel atk-devel gdk-pixbuf2-devel ghc-gi-gdk-devel
```

Install the nightly build of Rust.
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
Start by selecting 2 to customize the installation. Press enter for ```Default triple host```, then type ```nightly``` and press enter for the next option. Then, just press enter for all other options and the installation should begin.

Clone and build EWW.
```bash
cd ~/.src
git clone https://github.com/elkowar/eww.git && cd eww
cargo build --release -j $(nproc)
sudo mv target/release/eww /usr/bin/eww && sudo chmod +x /usr/bin/eww
```

### Install [WezTerm](https://wezfurlong.org/wezterm/)

Install dependencies.
```bash
sudo dnf install openssl
```

Get the rpm from the [official website](https://wezfurlong.org/wezterm/install/linux.html) and install it with:
```bash
sudo rpm -i {filename}.rpm
```

### Install additional software

Software form rpm fusion repositories.
```bash
sudo dnf install awesome devilspie2 rofi kitty ranger nemo nemo-fileroller nemo-image-converter neofetch xournalpp photoqt xclip krita blender mpv htop moc cava snapd fira-code-fonts bat numlockx lxappearance-obconf xbacklight lightdm-gtk-greeter-settings exa feh playerctl gnome-screenshot acpi maim lsd xdg-user-dirs slim dunst tint2 jgmenu rofi sxhkd lua starship fish gtk3 brightnessctl zsh pipewire pipewire-pulseaudio papirus-icon-theme curl jq libappindicator-gtk3 gnome-keyring light maim gpick mate-polkit yaru-sound-theme xdg-desktop-portal-gtk xdg-desktop-portal jgmenu lxappearance xfce4-power-manager xfce4-settings vdirsyncer khal moreutils fortune-mod wmctrl bc unzip bluez ImageMagick parcellite pavucontrol rofimoji NetworkManager blueman lm_sensors pipewire-utils inotify-tools xdotool ffmpeg upower python3-pip python3-devel

sudo dnf group install "Xfce Desktop" --skip-broken

sudo dnf remove blueberry
```

Flatpaks.
```bash
flatpak install flathub com.vscodium.codium
flatpak install flathub com.discordapp.Discord
flatpak install flathub org.telegram.desktop
flatpak install flathub com.spotify.Client
```

Snaps.
```bash
sudo ln -s /var/lib/snapd/snap /snap
sudo snap install jump
sudo snap install timg
```

Fish plugins.
```bash
fisher install edc/bass
```

Some extra configuration for ranger file manager.
```bash
pip3 install ueberzug
```

Set wezterm as the default terminal.
```bash
gsettings set org.cinnamon.desktop.default-applications.terminal exec wezterm
```

## Setup Visual Studio Codium

Copy the contents of the file ```.config/codium/settings.json``` to the ```.json``` configuration file from VSCodium.

Then, install all extensions present on ```.config/codium/codium-extensions```

## Build and install wifi receiver driver

Clone and build the package.
```bash
cd ~/.src
git clone https://github.com/aircrack-ng/rtl8812au.git
cd rtl88*
sudo make dkms_install
```

Now reboot the system for changes to take effect.

## System configuration

### Xfce session and startup

To use Xfce with the Awesome window manager, some services need to be disabled on startup. Login to Xfce to do this.

Under "current session", disable all programs, except:
- Xfce Settings Daemon
- Thunar
- msm_notifier
- Power Manager
- pulseaudio

Under "application startup", disable Xcape.

Remove all keyboard shortcuts from the keyboard section in settings manager.

Make lightdm the default display manager.
```bash
sudo systemctl disable gdm.service
sudo systemctl enable lightdm.service
```

### GIT setup

Change username and email.
```bash
git config --global user.name "Pedro Tonini Rosenberg Schneider"
git config --global user.email "pedrotrschneider@gmail.com"
```

Generate ssh keys.
```bash
ssh-keygen -t ed25519
```

## Squashing some bugs

### SELinux warnings

If you get some SELinux related warnings on the terminal, a workaround is to disable SELinux from the kernel params.
```bash
sudo grubby --update-kernel ALL --args selinux=0
```

If you need to re-enable SELinux for some reason:
```bash
grubby --update-kernel ALL --remove-args selinux
```

### Backlight control not working

If the function keys for backlight control don't work, try to run the command from the terminal.
```bash
xbacklight -inc 10
```

If you get an error such as
```
No outputs have backlight property
```
then create do the following:
```bash
sudo nano /etc/X11/xorg.conf
```

and append these lines to the file:
```
Section "Device"
    Identifier  "Intel Graphics" 
    Driver      "intel"
    Option      "Backlight"  "intel_backlight"
EndSection
```

Now, log out and log in again for changes to take effect.

### Ranger icons not working

If you find that ranger icons are now showing, try switching wezterm's font to ```DejaVu Sans``` and check if it works. If it does, you should be able to switch back to whatever font you had before and it'll still work.

## Resources

- [Pokeget](https://github.com/talwat/pokeget)
- [Awesome window manager widgets](https://github.com/streetturtle/awesome-wm-widgets)

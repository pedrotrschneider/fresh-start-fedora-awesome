#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

# Compositor stuff
run devilspie2
run picom --experimental-backends

# Some system tray stuff and xfce-session
run xfce4-session
run numlockx on
sh ~/.config/eww/scripts/init-eww.sh &

# Wallpaper
feh --bg-fill ~/Pictures/wallpapers/wallpaper-nordic.jpg

# Some daemons
libinput-gestures-setup start


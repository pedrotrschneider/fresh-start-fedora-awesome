local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local dpi   = require("beautiful.xresources").apply_dpi

local math, string, os = math, string, os
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility

local theme                                     = {}

theme.dir                                       = os.getenv("HOME") .. "/.config/awesome/themes/nord"
theme.wallpaper                                 = os.getenv("HOME") .. "/wallpaper.jpg"

theme.font                                      = "Noto Sans Regular 11"
theme.taglist_font                              = "Noto Sans Regular 16"

theme.fg_normal                                 = "#FEFEFE"
theme.fg_focus                                  = "#88C0D0"
theme.fg_urgent                                 = "#B74822"

theme.bg_normal                                 = "#3B4252"
theme.bg_focus                                  = "#3B4252"
theme.bg_urgent                                 = "#3B4252"

theme.taglist_bg_focus                          = "#00000000"
theme.taglist_fg_focus                          = "#B48EAD"
theme.taglist_fg_occupied                       = "#81A1C1"
theme.taglist_fg_urgent                         = "#BF616A"
theme.taglist_fg_empty                          = "#4C566A"

theme.tasklist_bg_focus                         = "#1A1B26"
theme.tasklist_fg_focus                         = "#889FA7"

theme.border_width                              = dpi(2)
theme.border_normal                             = "#3B4252"
theme.border_focus                              = "#81A1C1"
theme.border_marked                             = "#3B4252"

theme.menu_height                               = dpi(25)
theme.menu_width                                = dpi(260)
theme.menu_submenu_icon                         = theme.dir .. "/icons/submenu.png"

theme.awesome_icon                              = theme.dir .. "/icons/awesome.png"

theme.layout_tile                               = theme.dir .. "/icons/tile.png"
theme.layout_tileleft                           = theme.dir .. "/icons/tileleft.png"
theme.layout_tilebottom                         = theme.dir .. "/icons/tilebottom.png"
theme.layout_tiletop                            = theme.dir .. "/icons/tiletop.png"
theme.layout_fairv                              = theme.dir .. "/icons/fairv.png"
theme.layout_fairh                              = theme.dir .. "/icons/fairh.png"
theme.layout_spiral                             = theme.dir .. "/icons/spiral.png"
theme.layout_dwindle                            = theme.dir .. "/icons/dwindle.png"
theme.layout_max                                = theme.dir .. "/icons/max.png"
theme.layout_fullscreen                         = theme.dir .. "/icons/fullscreen.png"
theme.layout_magnifier                          = theme.dir .. "/icons/magnifier.png"
theme.layout_floating                           = theme.dir .. "/icons/floating.png"

theme.tasklist_plain_task_name                  = true
theme.tasklist_disable_icon                     = true

theme.useless_gap                               = dpi(4)

local markup = lain.util.markup
local separators = lain.util.separators

function theme.at_screen_connect(s)
    -- All tags open with layout 1
    awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()

    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(my_table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

    local systray = wibox.widget.systray({
        forced_height = dpi(20),
        visible = false,
    })

    -- Create the wiboxes
    s.mywibox = awful.wibar({
        position = "top",
        screen = s,
        height = dpi(40),
        border_width = dpi(8),
        bg = "#00000000",
        fg = "#00000000",
    })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        {
            layout = wibox.layout.fixed.horizontal,
            wibox.container.background(
                wibox.container.margin(
                    wibox.widget {
                        s.mytaglist,
                        s.mypromptbox,
                        layout = wibox.layout.align.horizontal
                    },
                    dpi(8),
                    dpi(8)
                ),
                theme.bg_normal,
                gears.shape.rounded_rect
            ),
        },

        {
            layout = wibox.layout.fixed.horizontal,
        },

        {
            layout = wibox.layout.fixed.horizontal,
            wibox.container.background(
                wibox.container.margin(
                    wibox.widget {
                        -- wibox.widget.systray(),
                        systray,
                        s.mylayoutbox,
                        layout = wibox.layout.align.horizontal
                    },
                    dpi(8),
                    dpi(8),
                    dpi(8),
                    dpi(8)
                ),
                theme.bg_normal,
                gears.shape.rounded_rect
            ),
        },
    }
end

return theme

#! /bin/bash

if [ $1 = "up" ]
then
    light -A 10
elif [ $1 = "down" ]
then
    light -U 10
fi

BRIGHTNESS=$(light)
eww update brightness=$BRIGHTNESS
eww close volume-indicator
eww close mic-indicator
eww open brightness-indicator
killall queue-close-indicators.sh
~/.config/awesome/scripts/queue-close-indicators.sh &

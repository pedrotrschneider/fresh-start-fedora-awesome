--[[

     Awesome WM configuration template
     https://github.com/awesomeWM

     Freedesktop : https://github.com/lcpz/awesome-freedesktop

     Copycats themes : https://github.com/lcpz/awesome-copycats

     lain : https://github.com/lcpz/lain

--]]

-- {{{ Required libraries
local awesome, client, mouse, screen, tag = awesome, client, mouse, screen, tag
local ipairs, string, os, table, tostring, tonumber, type = ipairs, string, os, table, tostring, tonumber, type

--https://awesomewm.org/doc/api/documentation/05-awesomerc.md.html
-- Standard awesome library
local gears         = require("gears") --Utilities such as color parsing and objects
local awful         = require("awful") --Everything related to window managment
                      require("awful.autofocus")
-- Widget and layout library
local wibox         = require("wibox")

-- Theme handling library
local beautiful     = require("beautiful")

-- Notification library
local naughty       = require("naughty")
naughty.config.defaults['icon_size'] = 100

--local menubar       = require("menubar")

local lain          = require("lain")
local freedesktop   = require("freedesktop")

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
local hotkeys_popup = require("awful.hotkeys_popup").widget
                      require("awful.hotkeys_popup.keys")
local my_table      = awful.util.table or gears.table -- 4.{0,1} compatibility
local dpi           = require("beautiful.xresources").apply_dpi
-- }}}



-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}


-- {{{ Autostart windowless processes
local function run_once(cmd_arr)
    for _, cmd in ipairs(cmd_arr) do
        awful.spawn.with_shell(string.format("pgrep -u $USER -fx '%s' > /dev/null || (%s)", cmd, cmd))
    end
end

run_once({ "unclutter -root" }) -- entries must be comma-separated
-- }}}

-- {{{ Variable definitions
local theme = "nord"

local theme_path = string.format("%s/.config/awesome/themes/%s/theme.lua", os.getenv("HOME"), theme)
beautiful.init(theme_path)

local modkey        = "Mod4"
local altkey        = "Mod1"
local ctrlkey       = "Control"
local shiftkey      = "Shift"

-- personal variables
local browser           = "firefox"
local editor            = os.getenv("EDITOR") or "nano"
local editorgui         = "codium"
local filemanager       = "nemo"
local terminal          = "wezterm"
local virtualmachine    = "virtualbox"

-- awesome variables
awful.util.terminal = terminal
awful.util.tagnames = { "⏺", "⏺", "⏺", "⏺", "⏺", "⏺", "⏺", "⏺", "⏺"}

awful.layout.suit.tile.left.mirror = true
awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.floating,
    -- awful.layout.suit.tile.top,
    -- awful.layout.suit.tile.left,
    -- awful.layout.suit.tile.bottom,
    -- awful.layout.suit.fair,
    -- awful.layout.suit.fair.horizontal,
    -- awful.layout.suit.spiral,
    -- awful.layout.suit.spiral.dwindle,
    -- awful.layout.suit.max,
    -- awful.layout.suit.max.fullscreen,
    -- awful.layout.suit.magnifier,
    -- awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
    -- lain.layout.cascade,
    -- lain.layout.cascade.tile,
    -- lain.layout.centerwork,
    -- lain.layout.centerwork.horizontal,
    -- lain.layout.termfair,
    -- lain.layout.termfair.center,
}

awful.util.taglist_buttons = my_table.join(
    awful.button({ }, 1, function(t) t:view_only() end),
    awful.button({ modkey }, 1, function(t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),
    awful.button({ }, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, function(t)
        if client.focus then
            client.focus:toggle_tag(t)
        end
    end),
    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
)

awful.util.tasklist_buttons = my_table.join(
    awful.button({ }, 1, function (c)
        if c == client.focus then
            c.minimized = true
        else
            -- Without this, the following
            c.minimized = false
            if not c:isvisible() and c.first_tag then
                c.first_tag:view_only()
            end
            -- This will also un-minimize
            -- the client, if needed
            client.focus = c
            c:raise()
        end
    end),
    awful.button({ }, 3, function()
        local instance = nil

        return function()
            if instance and instance.wibox.visible then
                instance:hide()
                instance = nil
            else
                instance = awful.menu.clients({theme = {width = dpi(250)}})
            end
        end
    end),
    awful.button({ }, 4, function() awful.client.focus.byidx(1) end),
    awful.button({ }, 5, function() awful.client.focus.byidx(-1) end)
)

lain.layout.termfair.nmaster           = 3
lain.layout.termfair.ncol              = 1
lain.layout.termfair.center.nmaster    = 3
lain.layout.termfair.center.ncol       = 1
lain.layout.cascade.tile.offset_x      = dpi(2)
lain.layout.cascade.tile.offset_y      = dpi(32)
lain.layout.cascade.tile.extra_padding = dpi(5)
lain.layout.cascade.tile.nmaster       = 5
lain.layout.cascade.tile.ncol          = 2
-- }}}

-- {{{ Menu
local myawesomemenu = {
    { "hotkeys", function() return false, hotkeys_popup.show_help end },
    { "arandr", "arandr" },
}

awful.util.mymainmenu = freedesktop.menu.build({
    before = {
        { "Awesome", myawesomemenu },
        --{ "Atom", "atom" },
        -- other triads can be put here
    },
    after = {
        { "Terminal", terminal },
        { "Log out", function() awesome.quit() end },
        { "Sleep", "systemctl suspend" },
        { "Restart", "systemctl reboot" },
        { "Shutdown", "systemctl poweroff" },
        -- other triads can be put here
    }
})

-- {{{ Screen

-- Create a wibox for each screen and add it
awful.screen.connect_for_each_screen(function(s) beautiful.at_screen_connect(s)
    s.systray = wibox.widget.systray()
    s.systray.visible = true
 end)
-- }}}

-- {{{ Mouse bindings
root.buttons(my_table.join(
    awful.button({ }, 3, function() awful.util.mymainmenu:toggle() end)
))
-- }}}

-- {{{ Key bindings
globalkeys = my_table.join(

    -- {{{ Personal keybindings
    -- super + ...
    awful.key( -- Rofi theme selector
        { modkey }, "r",
        function() awful.util.spawn( "rofi-theme-selector" ) end,
        {description = "rofi theme selector", group = "super"}
    ),
    awful.key( -- Rofi launcher
        { modkey }, "space", 
        function() awful.util.spawn( "rofi -show drun -show-icons" ) end,
        {description = "rofi drun", group = "super"}
    ),
    awful.key( -- Rofi window selector
        { modkey, altkey }, "space", 
        function() awful.util.spawn( "rofi -show window -show-icons" ) end,
        {description = "rofi show opened windows", group = "super"}
    ),
    awful.key( -- Rofi powermenu
        { modkey }, "x", 
        function() awful.util.spawn( "./.config/rofi/powermenu.sh" ) end,
        {description = "exit", group = "hotkeys"}
    ),
    awful.key( -- Launch terminal
        { modkey }, "Return",
        function() awful.util.spawn( terminal ) end,
        {description = "kitty", group = "super"}
    ),
    awful.key( -- Launch browser
        { modkey }, "b", 
        function() awful.util.spawn( browser ) end,
        {description = "browser", group = "super"}
    ),
    awful.key( -- Pipewire audio control
        { modkey }, "v", 
        function() awful.util.spawn( "pavucontrol" ) end,
        {description = "pipewireaudio control", group = "super"}
    ),
    awful.key( -- Xkill
        { modkey }, "Escape", 
        function() awful.util.spawn( "xkill" ) end,
        {description = "Kill proces", group = "hotkeys"}
    ),
        
    -- screenshots
    awful.key( -- Capture active screen
        { }, "Print", 
        function() awful.util.spawn("glorious-screenshot") end,
        {description = "Capture active screen", group = "screenshots"}
    ),
    awful.key( -- Capture active screen and copy to clipboard
        { ctrlkey }, "Print", 
        function() awful.util.spawn("glorious-screenshot -c") end,
        {description = "Capture active screen and copy to clipboard", group = "screenshots"}
    ),
    awful.key( -- Capture active screen with 5 seconds delay
        { altkey }, "Print", 
        function() awful.util.spawn("glorious-screenshot -d 5") end,
        {description = "Capture active screen with 5 seconds delay", group = "screenshots"}
    ),
    awful.key( -- Capture active screen with 5 seconds delay and copy to the clipboard
        { ctrlkey, altkey }, "Print", 
        function() awful.util.spawn("glorious-screenshot -c -d 5") end,
        {description = "Capture active screen with 5 seconds delay and copy to the clipboard", group = "screenshots"}
    ),
    awful.key( -- Capture selected area
        { shiftkey }, "Print", 
        function() awful.util.spawn("glorious-screenshot -s") end,
        {description = "Capture selected area", group = "screenshots"}
    ),
    awful.key( -- Capture selected area and copy to clipboard
        { ctrlkey, shiftkey }, "Print", 
        function() awful.util.spawn("glorious-screenshot -s -c") end,
        {description = "Capture selected area and copy to clipboard", group = "screenshots"}
    ),
    awful.key( -- Capture selected area with 5 seconds delay
        { altkey, shiftkey }, "Print", 
        function() awful.util.spawn("glorious-screenshot -s -d 5") end,
        {description = "Capture selected area with 5 seconds delay", group = "screenshots"}
    ),
    awful.key( -- Capture selected area with 5 seconds delay and copy to clipboard 
        { ctrlkey, altkey, shiftkey }, "Print", 
        function() awful.util.spawn("glorious-screenshot -s -d 5") end,
        {description = "Capture selected area with 5 seconds delay and copy to clipboard", group = "screenshots"}
    ),

    -- Personal keybindings}}}

    -- Hotkeys Awesome
    awful.key( -- Hotkeys popup
        { modkey, }, "s",
        hotkeys_popup.show_help,
        {description = "Show hotkey popup", group="awesome"}
    ),
    awful.key( -- Toggle top bar
        { modkey }, "i",
        function()
            for s in screen do
                s.mywibox.visible = not s.mywibox.visible
            end
            awful.util.spawn("eww open --toggle bar")
        end,
        {description = "Toggle top bar", group = "awesome"}
    ),

    -- Tag browsing with modkey
    awful.key( -- Go to previous tag
        { modkey, ctrlkey }, "Down",
        awful.tag.viewprev,
        {description = "Go to previous tag", group = "tag"}
    ),
    awful.key( -- Go to next tag
        { modkey, ctrlkey }, "Up",
        awful.tag.viewnext,
        {description = "Go to next tag", group = "tag"}
    ),

    -- By direction client focus
    awful.key( -- Focus window down
        { modkey }, "j",
        function()
            awful.client.focus.global_bydirection("down")
            if client.focus then client.focus:raise() end
        end,
        {description = "Focus window down", group = "client"}
    ),
    awful.key( -- Focus window up
        { modkey }, "k",
        function()
            awful.client.focus.global_bydirection("up")
            if client.focus then client.focus:raise() end
        end,
        {description = "Focus window up", group = "client"}
    ),
    awful.key( -- Focus window left
        { modkey }, "h",
        function()
            awful.client.focus.global_bydirection("left")
            if client.focus then client.focus:raise() end
        end,
        {description = "Focus window left", group = "client"}
    ),
    awful.key( -- Focus window right
        { modkey }, "l",
        function()
            awful.client.focus.global_bydirection("right")
            if client.focus then client.focus:raise() end
        end,
        {description = "Focus window right", group = "client"}
    ),

    -- By direction client focus with arrows
    awful.key( -- Focus window down
        { modkey }, "Down",
        function()
            awful.client.focus.global_bydirection("down")
            if client.focus then client.focus:raise() end
        end,
        {description = "Focus window down", group = "client"}
    ),
    awful.key( -- Focus window up
        { modkey }, "Up",
        function()
            awful.client.focus.global_bydirection("up")
            if client.focus then client.focus:raise() end
        end,
        {description = "Focus window up", group = "client"}
    ),
    awful.key( -- Focus window left
        { modkey }, "Left",
        function()
            awful.client.focus.global_bydirection("left")
            if client.focus then client.focus:raise() end
        end,
        {description = "Focus window left", group = "client"}
    ),
    awful.key( -- Focus window left
        { modkey }, "Right",
        function()
            awful.client.focus.global_bydirection("right")
            if client.focus then client.focus:raise() end
        end,
        {description = "focus right", group = "client"}
    ),

    -- Layout manipulation
    awful.key( -- Swap with next client by index
        { modkey, shiftkey }, "j",
        function() awful.client.swap.byidx(1) end,
        {description = "Swap with next client by index", group = "client"}
    ),
    awful.key( -- Swap with previous client by index
        { modkey, shiftkey }, "k",
        function() awful.client.swap.byidx(-1) end,
        {description = "Swap with previous client by index", group = "client"}
    ),
    awful.key( -- Focus the next screen
        { modkey, ctrlkey }, "j",
        function() awful.screen.focus_relative(1) end,
        {description = "Focus the next screen", group = "screen"}
    ),
    awful.key( -- Focus the previous screen
        { modkey, ctrlkey }, "k",
        function() awful.screen.focus_relative(-1) end,
        {description = "focus the previous screen", group = "screen"}
    ),
    awful.key( -- Increase master width factor
        { modkey, shiftkey }, "h",
        function() awful.tag.incmwfact(0.01) end,
        {description = "Increase master width factor", group = "layout"}
    ),
    awful.key( -- Decrease master width factor
        { modkey, shiftkey }, "l",
        function() awful.tag.incmwfact(-0.01) end,
        {description = "Decrease master width factor", group = "layout"}
    ),
    awful.key( -- Increase the number of columns
        { modkey, ctrlkey }, "h",
        function() awful.tag.incnmaster(1, nil, true) end,
        {description = "Increase the number of columns", group = "layout"}
    ),
    awful.key( -- Decrease the number of columns
        { modkey, ctrlkey }, "l",
        function() awful.tag.incnmaster(-1, nil, true) end,
        {description = "Decrease the number of columns", group = "layout"}
    ),
    awful.key( -- Jump to urgent client
        { modkey }, "u",
        awful.client.urgent.jumpto,
        {description = "Jump to urgent client", group = "client"}
    ),
    
    -- Layout manipulation with arrow keys
    awful.key( -- Swap with next client by index
        { modkey, shiftkey }, "Down",
        function() awful.client.swap.byidx(1) end,
        {description = "Swap with next client by index", group = "client"}),
    awful.key( -- Focus the next screen
        { modkey, shiftkey }, "Up",
        function() awful.client.swap.byidx(-1) end,
        {description = "Swap with previous client by index", group = "screen"}),
    awful.key( -- Focus the next screen
        { modkey, ctrlkey }, "Down",
        function() awful.screen.focus_relative(1) end,
        {description = "Focus the next screen", group = "screen"}),
    awful.key( -- Focus the previous screen
        { modkey, ctrlkey }, "Up",
        function() awful.screen.focus_relative(-1) end,
        {description = "Focus the previous screen", group = "screen"}),
    awful.key( -- Increase master width factor
        { modkey, shiftkey }, "Right",
        function() awful.tag.incmwfact(0.01) end,
        {description = "Increase master width factor", group = "layout"}),
    awful.key( -- Decrease master width factor
        { modkey, shiftkey }, "Left",
        function() awful.tag.incmwfact(-0.01) end,
        {description = "Decrease master width factor", group = "layout"}),
    awful.key( -- Increase the number of columns
        { modkey, ctrlkey }, "Left",
        function() awful.tag.incnmaster(1, nil, true) end,
        {description = "Increase the number of columns", group = "layout"}),
    awful.key( -- Decrease the number of columns
        { modkey, ctrlkey }, "Right",
        function() awful.tag.incnmaster(-1, nil, true) end,
        {description = "Decrease the number of columns", group = "layout"}),

    -- Show/Hide Systray
    awful.key( -- Toggle systray visibility
        { modkey }, "-",
        function()
            awful.screen.focused().systray.visible = not awful.screen.focused().systray.visible
        end,
        {description = "Toggle systray visibility", group = "awesome"}
    ),

    -- Dynamic tagging
    awful.key( -- Add new tag
        { modkey, shiftkey }, "n",
        function() lain.util.add_tag() end,
        {description = "Add new tag", group = "tag"}
    ),
    awful.key( -- Rename tag
        { modkey, ctrlkey }, "r",
        function() lain.util.rename_tag() end,
        {description = "Rename tag", group = "tag"}
    ),
    awful.key( -- Move tag to the left
        { modkey, altkey }, "Down",
        function() lain.util.move_tag(-1) end,
        {description = "Move tag to the left", group = "tag"}
    ),
    awful.key( -- Move tag to the right
        { modkey, altkey }, "Up",
        function() lain.util.move_tag(1) end,
        {description = "Move tag to the right", group = "tag"}
    ),
    awful.key( -- Delete tag
        { modkey, shiftkey }, "y",
        function() lain.util.delete_tag() end,
        {description = "Delete tag", group = "tag"}
    ),
    
    -- Standard program
    awful.key( -- Reload awesome
        { modkey, shiftkey }, "r",
        awesome.restart,
        {description = "Reload awesome", group = "awesome"}
    ),
    awful.key( -- Quit awesome
        { modkey, shiftkey }, "x",
        awesome.quit,
        {description = "Quit awesome", group = "awesome"}
    ),
    awful.key( -- Select next
        { modkey, shiftkey }, "space",
        function() awful.layout.inc( 1) end,
        {description = "Select next", group = "layout"}
    ),
    awful.key( -- Restore minimized
        { modkey, ctrlkey }, "n",
        function()
            local c = awful.client.restore()
            -- Focus restored client
            if c then
                client.focus = c
                c:raise()
            end
        end,
        {description = "Restore minimized", group = "client"}
    ),

    -- Brightness controll
    awful.key( -- Brightness +10%
        { }, "XF86MonBrightnessUp",
        function() os.execute("~/.config/awesome/scripts/brightness.sh up") end,
        {description = "Brightness +10%", group = "hotkeys"}
    ),
    awful.key( -- Brightness -10%
        { }, "XF86MonBrightnessDown",
        function() os.execute("~/.config/awesome/scripts/brightness.sh down") end,
        {description = "Brightness -10%", group = "hotkeys"}
    ),

    -- ALSA volume control
    -- With keyboard shortcuts
    awful.key(
        { ctrlkey, altkey }, "Up",
        function() os.execute("~/.config/awesome/scripts/volume.sh up") end,
        {description = "Volume +5%", group = "hotkeys"}
    ),
    awful.key(
        { ctrlkey, altkey }, "Down",
        function() os.execute("~/.config/awesome/scripts/volume.sh down") end,
        {description = "Volume -5%", group = "hotkeys"}
    ),
    awful.key(
        { ctrlkey, altkey }, "0",
        function() os.execute("~/.config/awesome/scripts/volume.sh toggle") end,
        {description = "Mute volume", group = "hotkeys"}
    ),
    awful.key(
        { shiftkey, altkey }, "Up",
        function() os.execute("~/.config/awesome/scripts/mic.sh up") end,
        {description = "Mic +5%", group = "hotkeys"}
    ),
    awful.key(
        { shiftkey, altkey }, "Down",
        function() os.execute("~/.config/awesome/scripts/mic.sh down") end,
        {description = "Mic -5%", group = "hotkeys"}
    ),
    awful.key(
        { shiftkey, altkey }, "0",
        function() os.execute("~/.config/awesome/scripts/mic.sh toggle") end,
        {description = "Mute mic", group = "hotkeys"}
    ),
    
    -- With function keys
    awful.key( -- Volume +5%
        { }, "XF86AudioRaiseVolume",
        function() os.execute("~/.config/awesome/scripts/volume.sh up") end,
        {description = "Volume +5%", group = "hotkeys"}
    ),
    awful.key( -- Volume -5%
        { }, "XF86AudioLowerVolume",
        function() os.execute("~/.config/awesome/scripts/volume.sh down") end,
        {description = "Volume -5%", group = "hotkeys"}
    ),
    awful.key( -- Mute volume
        { }, "XF86AudioMute",
        function() os.execute("~/.config/awesome/scripts/volume.sh toggle") end,
        {description = "Mute volume", group = "hotkeys"}
    ),

    -- MPRIS media keys
    awful.key( -- MPRIS play/pause
        { }, "XF86AudioPlay",
        function() awful.util.spawn("playerctl play-pause", false) end,
        {description = "MPRIS play/pause", group = "hotkeys"}
    ),
    awful.key( -- MPRIS next
        { }, "XF86AudioNext",
        function() awful.util.spawn("playerctl next", false) end,
        {description = "MPRIS next", group = "hotkeys"}
    ),
    awful.key( -- MPRIS previous
        { }, "XF86AudioPrev",
        function() awful.util.spawn("playerctl previous", false) end,
        {description = "MPRIS previous", group = "hotkeys"}
    ),
    awful.key( -- MPRIS stop
        { }, "XF86AudioStop",
        function() awful.util.spawn("playerctl stop", false) end,
        {description = "MPRIS stop", group = "hotkeys"}
    )
)

clientkeys = my_table.join(
    awful.key( -- Magnify active client
        { altkey, shiftkey }, "m",
        lain.util.magnify_client,
        {description = "Magnify active client", group = "client"}
    ),
    awful.key( -- Toggle fullscreen on active client
        { modkey }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "Toggle fullscreen on active client", group = "client"}
    ),
    awful.key( -- Close active client
        { modkey }, "q",
        function (c) c:kill() end,
        {description = "Close active client", group = "client"}
    ),
    awful.key( -- Toggle floating on active client
        { modkey, shiftkey }, "space",
        awful.client.floating.toggle,
        {description = "Toggle floating on active client", group = "client"}
    ),
    awful.key( -- Move active client to master
        { modkey, ctrlkey }, "Return",
        function (c) c:swap(awful.client.getmaster()) end,
        {description = "Move active client to master", group = "client"}
    ),
    awful.key( -- Move active client to screen #
        { modkey }, "o",
        function (c) c:move_to_screen() end,
        {description = "Move active client to screen #", group = "client"}
    ),
    awful.key( -- Toggle always on top on active client
        { modkey }, "t",
        function (c) c.ontop = not c.ontop end,
        {description = "Toggle always on top on active client", group = "client"}
    ),
    awful.key( -- Minimize active client
        { modkey }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "Minimize active client", group = "client"}
    ),

    awful.key( -- Maximize active client
        { modkey }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "Maximize active client", group = "client"}
    )
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9 and to numpad
keypad_numbers = {"KP_End", "KP_Down", "KP_Next", "KP_Left", "KP_Begin", "KP_Right", "KP_Home", "KP_Up", "KP_Page_Up"};

for i = 1, 9 do
    -- Hack to only show tags 1 and 9 in the shortcut window (mod+s)
    local descr_view, descr_toggle, descr_move, descr_toggle_focus
    if i == 1 or i == 9 then
        descr_view = {description = "Go to tag #", group = "tag"}
        descr_toggle = {description = "Toggle tag #", group = "tag"}
        descr_move = {description = "Move focused client to tag #", group = "tag"}
        descr_toggle_focus = {description = "Toggle focused client on tag #", group = "tag"}
    end
    globalkeys = my_table.join(globalkeys,
        -- View tag only.
        awful.key( -- Go to tag # with num-row
            { modkey }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    tag:view_only()
                end
            end,
            descr_view
        ),
        awful.key( -- Go to tag # with num-pad
            { modkey }, keypad_numbers[i],
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    tag:view_only()
                end
            end,
            descr_view
        ),
        -- Toggle tag display.
        awful.key( -- Toggle tag # with num-row
            { modkey, ctrlkey }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end,
            descr_toggle
        ),
        awful.key( -- Toggle tag # with num-pad
            { modkey, ctrlkey }, keypad_numbers[i],
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end,
            descr_toggle
        ),
        -- Move client to tag.
        awful.key( -- Move focused client to tag # with num-row
            { modkey, shiftkey }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                        tag:view_only()
                    end
                end
            end,
            descr_move
        ),
        awful.key( -- Move focused client to tag # with num-pad
            { modkey, shiftkey }, keypad_numbers[i],
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                        tag:view_only()
                    end
                end
            end,
            descr_move
        ),
        -- Toggle tag on focused client.
        awful.key( -- Toggle focused client on tag # with num-row
            { modkey, ctrlkey, shiftkey }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            descr_toggle_focus
        ),
        awful.key( -- Toggle focused client on tag # with num-pad
            { modkey, ctrlkey, shiftkey }, keypad_numbers[i],
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            descr_toggle_focus
        )
    )
end

clientbuttons = gears.table.join(
    awful.button(
        { }, 1,
        function (c)
            c:emit_signal("request::activate", "mouse_click", {raise = true})
        end
    ),
    awful.button(
        { modkey }, 1,
        function (c)
            c:emit_signal("request::activate", "mouse_click", {raise = true})
            awful.mouse.client.move(c)
        end
    ),
    awful.button(
        { modkey }, 3,
        function (c)
            c:emit_signal("request::activate", "mouse_click", {raise = true})
            awful.mouse.client.resize(c)
        end
    )
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { 
        rule = { },
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap+awful.placement.no_offscreen,
            size_hints_honor = false
        }
    },

    -- Titlebars
    {
        rule_any = { type = { "dialog", "normal" } },
        properties = { titlebars_enabled = false }
    },

    -- Set applications to be maximized at startup.
    -- find class or role via xprop command

    { -- Rules for gimp
        rule = { class = "Gimp*", role = "gimp-image-window" },
        properties = { maximized = true }
    },

    { -- Rules for inkscape
        rule = { class = "inkscape" },
        properties = { maximized = true }
    },

    { -- Rules for vlc
        rule = { class = "Vlc" },
        properties = { maximized = true }
    },

    -- Floating clients.
    {
        rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
        },
        class = {
          "Arandr",
          "Blueberry",
          "Galculator",
          "Gnome-font-viewer",
          "Gpick",
          "Imagewriter",
          "Font-manager",
          "Kruler",
          "MessageWin",  -- kalarm.
          "arcolinux-logout",
          "Peek",
          "Skype",
          "System-config-printer.py",
          "Sxiv",
          "Unetbootin.elf",
          "Wpa_gui",
          "pinentry",
          "veromix",
          "xtightvncviewer",
          "Xfce4-terminal"
        },
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
          "Preferences",
          "setup",
        }
      }, properties = { floating = true },
            callback = function (c)
                awful.placement.centered(c,nil)
            end },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- Autostart applications
awful.spawn.with_shell("~/.config/awesome/autostart.sh")
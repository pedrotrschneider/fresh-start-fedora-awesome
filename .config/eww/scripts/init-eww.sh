#! /bin/bash

# Starts the bar
eww open bar

# Updtaes output volume
VOLUME=$(amixer sget Master | awk -F'[][]' '/Right:|Mono:/ && NF > 1 {sub(/%/, ""); printf "%0.0f\n", $2}')
eww update volume=$VOLUME

# Updates output mute/unmute
WCV=$(amixer sget Master | grep "\[on\]" | wc -c)
if [ $WCV != 0 ]
then
    eww update volume-icon="icons/volume.svg"
else
    eww update volume-icon="icons/volume-mute.svg"
fi

# Updates input volume
VOLUME=$(amixer sget Capture | awk -F'[][]' '/Right:|Mono:/ && NF > 1 {sub(/%/, ""); printf "%0.0f\n", $2}')
eww update mic=$VOLUME

# Updates input mute/unmute
WCV=$(amixer sget Capture | grep "\[on\]" | wc -c)
if [ $WCV != 0 ]
then
    eww update mic-icon="icons/mic.svg"
else
    eww update mic-icon="icons/mic-mute.svg"
fi

# Updates brightness
BRIGHTNESS=$(light)
eww update brightness=$BRIGHTNESS
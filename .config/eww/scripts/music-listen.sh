#! /bin/bash

stdbuf -oL playerctl --follow status | xargs -n1 ~/.config/eww/scripts/music-process.sh set status 2> /dev/null &
stdbuf -oL playerctl --follow metadata | xargs -n1 ~/.config/eww/scripts/music-process.sh set metadata 2> /dev/null &
#! /bin/bash

if [ $1 = "set" ]
then
    if [ $2 = "status" ]
    then
        if [ $3 = "Playing" ]
        then
            eww update playback-icon="icons/pause.svg"
        else
            eww update playback-icon="icons/play.svg"
        fi
    elif [ $2 = "metadata" ]
    then
        eww update album-cover="$(playerctl metadata 'mpris:artUrl')"
        eww update music-title="$(playerctl metadata 'xesam:title' 2> /dev/null || echo 'No song playing')"
        eww update music-album="$(playerctl metadata 'xesam:album' 2> /dev/null || echo 'No song playing')"
        eww update music-artist="$(playerctl metadata 'xesam:artist' 2> /dev/null || echo 'No song playing')"
    fi
fi

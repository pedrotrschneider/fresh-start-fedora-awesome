#! /bin/bash

if [ $1 = "toggle" ]
then
    playerctl play-pause
elif [ $1 = "next" ]
then
    playerctl next
elif [ $1 = "previous" ]
then
    playerctl previous
elif [ $1 = "status" ]
then
    STATUS=$(playerctl status 2> /dev/null || echo "paused")
    if [ $STATUS = "Playing" ]
    then
        echo "icons/pause.svg"
    else
        echo "icons/play.svg"
    fi
elif [ $1 = "cover" ]
then
    rm /tmp/cover.png
    curl "$(playerctl metadata 'mpris:artUrl')" --output /tmp/cover.png > /dev/null
    echo /tmp/cover.png
elif [ $1 = "title" ]
then
    playerctl metadata 'xesam:title' 2> /dev/null || echo "No song playing"
elif [ $1 = "album" ]
then
    playerctl metadata 'xesam:album' 2> /dev/null || echo "No song playing"
elif [ $1 = "artist" ]
then
    playerctl metadata 'xesam:artist' 2> /dev/null || echo "No song playing"
fi
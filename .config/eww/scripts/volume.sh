#! /bin/bash

if [ $1 = "up" ]
then
    amixer set Master 5%+
elif [ $1 = "down" ]
then
    amixer set Master 5%-
elif [ $1 = "toggle" ]
then
    amixer set Master toggle
    WCV=$(amixer sget Master | grep "\[on\]" | wc -c)
    if [ $WCV != 0 ]
    then
        eww update volume-icon="icons/volume.svg"
    else
        eww update volume-icon="icons/volume-mute.svg"
    fi
fi

VOLUME=$(amixer sget Master | awk -F'[][]' '/Right:|Mono:/ && NF > 1 {sub(/%/, ""); printf "%0.0f\n", $2}')
eww update volume=$VOLUME
eww close mic-indicator
eww open volume-indicator
killall queue-close-indicators.sh
~/.config/awesome/scripts/queue-close-indicators.sh &
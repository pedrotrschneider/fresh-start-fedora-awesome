#! /bin/bash

if [ $1 = "up" ]
then
    amixer set Capture 5%+
elif [ $1 = "down" ]
then
    amixer set Capture 5%-
elif [ $1 = "toggle" ]
then
    amixer set Capture toggle
    WCV=$(amixer sget Capture | grep "\[on\]" | wc -c)
    if [ $WCV != 0 ]
    then
        eww update mic-icon="icons/mic.svg"
    else
        eww update mic-icon="icons/mic-mute.svg"
    fi
fi

VOLUME=$(amixer sget Capture | awk -F'[][]' '/Right:|Mono:/ && NF > 1 {sub(/%/, ""); printf "%0.0f\n", $2}')
eww update mic=$VOLUME
eww close volume-indicator
eww open mic-indicator
killall queue-close-indicators.sh
~/.config/awesome/scripts/queue-close-indicators.sh &
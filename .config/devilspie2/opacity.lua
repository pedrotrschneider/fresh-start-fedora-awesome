local trasnparent_apps = {
	"gedit",
	"Steam",
	"VSCodium",
	"pcmanfm",
	"Thunar",
	"Discord",
	"Telegram",
	"Alacritty",
	"kitty",
	"nemo"
}

local current_app = get_application_name()
print(current_app)
for _, app in ipairs(trasnparent_apps) do
	if string.find(current_app, app) then
		set_window_opacity(0.75)
		break
	end
end

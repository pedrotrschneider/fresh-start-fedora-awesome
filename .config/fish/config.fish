# Rust
set PATH $PATH ~/.cargo/bin

# Some non posix compliant stuff (remind me to switch to zsh some day)
bass source /etc/profile

# Ranger
alias ranger "ranger --choosedir=$HOME/.rangerdir && cd (cat ~/.rangerdir)"
alias r "ranger"

# Neofetch
alias neo="clear && neofetch"

# Git
alias gitcl="git clone"
alias gitst="git status"
alias gitpl="git pull"
alias gits="git status"
alias gita="git add"
alias gitc="git commit"
alias gitcm="git commit -m"
alias gitps="git push"
alias gitch="git checkout"

# Exa
alias ls="lsd"

function nvm
   bass source $HOME/.nvm/nvm.sh --no-use ';' nvm $argv
end

# rsync
alias cp="rsync -ahz --progress"

# codium
# alias codium "flatpak run com.vscodium.codium"

# theme
set -g fish_color_normal normal
set -g fish_color_command 81a1c1
set -g fish_color_quote a3be8c
set -g fish_color_redirection b48ead
set -g fish_color_end 88c0d0
set -g fish_color_error ebcb8b
set -g fish_color_param eceff4
set -g fish_color_comment 434c5e
set -g fish_color_match --background=brblue
set -g fish_color_selection white --bold --background=brblack
set -g fish_color_search_match bryellow --background=brblack
set -g fish_color_history_current --bold
set -g fish_color_operator 00a6b2
set -g fish_color_escape 00a6b2
set -g fish_color_cwd green
set -g fish_color_cwd_root red
set -g fish_color_valid_path --gnderline
set -g fish_color_autosuggestion 4c566a
set -g fish_color_user brgreen
set -g fish_color_host normal
set -g fish_color_cancel --reverse

set -g fish_pager_color_prefix normal --bold --gnderline
set -g fish_pager_color_progress brwhite --background=cyan
set -g fish_pager_color_completion normal
set -g fish_pager_color_description B3A06D
set -g fish_pager_color_selected_background --background=brblack

local wezterm = require 'wezterm';

return {
    font = wezterm.font("Fira Code"),
    font_size = 10.0,
    line_height = 1.1,
    color_scheme = "nord",
    enable_tab_bar = false,
    window_background_opacity = 0.7,
    window_padding = {
        left = 30,
        right = 30,
        top = 30,
        bottom = 30,
    }
}

@import "dark-horizon.rasi"

* {
    text-color          : @fg-color;
    background-color    : @bg-color;
    font                : "Roboto 16";
}

window {
    location            : center;
    y-offset            : -200;
    width               : 50%;
    border-radius       : 12;
    background-color    : @bg-color;
}

mainbox {
    spacing             : 0;
    background-color    : transparent;
    padding             : 16;
}

prompt {
    enabled             : true;
    padding             : 10px;
	background-color    : transparent;
	text-color          : @accent-fg-color;
}

textbox-prompt-colon {
	expand              : false;
	str                 : " System ";
	background-color    : transparent;
	text-color          : @accent-fg-color;
    padding             : 12px 10px 0px 10px;
}

inputbar {
	children            : [ textbox-prompt-colon, prompt ];
    spacing             : 0px;
    background-color    : @inputbar-bg-color;
    text-color          : @fg-color;
    expand              : false;
    border              : 2px;
    border-radius       : 8px;
    border-color        : @accent-bg-color;
    margin              : 0px 0px 0px 0px;
    padding             : 0px;
    position            : center;
}

entry {
    background-color    : inherit;
    placeholder         : "Search";
    placeholder-color   : @insensitive-fg-color;
}

case-indicator {
    background-color    : inherit;
}

message {
    padding             : 0;
    margin              : 16 0 0;
    border-radius       : 8;
    border-color        : @insensitive-bg-color;
    background-color    : @insensitive-bg-color;
}

textbox {
    padding             : 8 24;
    background-color    : inherit;
}

listview {
    lines               : 8;
    columns             : 1;
    fixed-height        : false;
    spacing             : 0;
    scrollbar           : false;
    background-color    : transparent;
    margin              : 16 0 0;
}

element {
    padding             : 8 16;
    background-color    : transparent;
    border-radius       : 8;
}

element normal normal {
}

element normal urgent {
}

element normal active {
    text-color          : @accent-bg-color;
}

element selected normal {
    background-color    : @accent-bg-color;
    text-color          : @accent-fg-color;
}

element selected urgent {
}

element selected active {
    background-color    : @accent-bg-color;
    text-color          : @accent-fg-color;
}

element-icon {
    size                : 1em;
    margin              : 0 8 0 0;
    background-color    : transparent;
}

element-text {
    background-color    : transparent;
}
#!/usr/bin/env bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x

rofi_command="rofi -theme ~/.config/rofi/nord-powermenu.rasi"

uptime=$(uptime -p | sed -e 's/up //g')
cpu=$($HOME/.config/rofi/bin/usedcpu)
memory=$($HOME/.config/rofi/bin/usedram)

# Options
shutdown="⏻    Shutdown"
reboot="↺    Reboot"
lock="🗝    Lock"
suspend="⏾    Suspend"
logout="⍈    Logout"

# Message
msg() {
	rofi -theme "$HOME/.config/rofi/message.rasi" -e "Available Options  -  yes / y / no / n"
}

# Variable passed to rofi
options="$shutdown\n$reboot\n$lock\n$suspend\n$logout"

chosen="$(echo -e "$options" | $rofi_command -p "UP - $uptime" -dmenu -selected-row 2)"
case $chosen in
    $shutdown)
		systemctl poweroff
        ;;
    $reboot)
		systemctl reboot
        ;;
    $lock)
    i3lock -i ~/Pictures/wallpapers/wallpaper-nordic.jpg -e
        ;;
    $suspend)
		xfce4-screensaver-command -a
    i3lock -i ~/Pictures/wallpapers/wallpaper-nordic.jpg -e
        ;;
    $logout)
		killall awesome
        ;;
esac
